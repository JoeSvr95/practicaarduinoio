#include <stdio.h>    // Standard input/output definitions
#include <stdlib.h>
#include <string.h>   // String function definitions
#include <unistd.h>   // para usleep()
#include <getopt.h>
#include <math.h>

#include "arduino-serial-lib.h"

float calculateSD(int data[]);
float calculateMean(int data[]);

void error(char* msg)
{
    fprintf(stderr, "%s\n",msg);
    exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
	int fd = -1;
	int baudrate = 9600;  // default
	char *buffer;	
	char *res;
	int temp=0;
	int hum=0;
	int counter=0;
	int data_t[12];
	int data_h[12];
	int i=0;
	int maxtemp = 0;
	int mintemp = 1000;
	int maxhum = 0;
	int minhum = 1000;
	fd = serialport_init("/dev/ttyACM0", baudrate);

	if( fd==-1 )
	{
		error("couldn't open port");
		return -1;
	}
	
	serialport_flush(fd);
	
	while(1){
		sleep(5);
		buffer="t";
		write(fd,buffer,1);
		usleep(50000);
		read(fd,&temp,1);
		printf("Info of temp: %d \n",temp);
		if (temp > maxtemp){
			maxtemp = temp;	
		}
		if (temp < mintemp){
			mintemp = temp;
		}
		data_t[i]=temp;		
		
		usleep(50000);
		buffer="h";
		write(fd,buffer,1);
		usleep(50000);
		read(fd,&hum,1);
		printf("Info of hum: %d \n",hum);
		if (hum > maxhum){
			maxhum = hum;	
		}
		if (hum < minhum){
			minhum = hum;	
		}
		data_h[i]=hum;
		counter+=5;
		if(counter==60){
			printf("Counter reached");
			printf("Standard deviation of humidity: %.2f",calculateSD(data_h));
			printf("Mean of humidity: %2.f", calculateMean(data_h));
			printf("Standard deviation of temp: %.2f", calculateSD(data_t));
			printf("Mean of temp: %2.f", calculateMean(data_t));
			printf("Maximum temperature: %d", maxtemp);
			printf("Minimum temperature: %d", mintemp);
			printf("Maximum humidity: %d", maxhum);
			printf("Minimum humidity: %d", minhum);
			i=0;
			counter=0;
			maxtemp = 0;
			mintemp = 1000;
			maxhum = 0;
			minhum = 1000; 
		}
		i++;
	}	
	
	close( fd );
	return 0;

}

/* Ejemplo para calcular desviacion estandar y media */
float calculateMean(int data[]){
 int sum = 0, standardDeviation = 0;
 float mean=0.0;

    int i;

    for(i = 0; i < 12; ++i)
    {
        sum += data[i];
    }

    mean = sum/12;

return mean;

}

float calculateSD(int data[])
{
    int sum = 0, mean, standardDeviation = 0;

    int i;

    for(i = 0; i < 10; ++i)
    {
        sum += data[i];
    }

    mean = sum/10;

    for(i = 0; i < 10; ++i)
        standardDeviation += pow(data[i] - mean, 2);

    return sqrt(standardDeviation / 10);
}


